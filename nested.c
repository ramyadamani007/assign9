/*****************
	Include
*****************/
#include<stdio.h>
#include<stdlib.h>

/***** Declaration of Structure 1*****/

struct information
{
/**** Variable Declaration ****/
	int age;
	char blood_group[15];
}i1;

/***** Declaration of Structure 2*****/


typedef struct patient 
{				
/**** Variable Declaration ****/
	int patientid;		
	char name[10];
	struct information i1;
}patient;

/**** MAIN ****/

int main()
{
	patient p1;
	struct information i1;
	printf("enter patient Id and name of the patient.\n");
	scanf("%d %s",&p1.patientid, &p1.name);
	printf("Enter the information like age and blood group of patient.\n");
	scanf("%d %s",&p1.i1.age, &p1.i1.blood_group);
	printf("The patient id is %d.\nThe patient name is %s.\nThe age of patient is %d.\nThe blood group of patient is %s.\n ",p1.patientid, p1.name, p1.i1.age, p1.i1.blood_group);
	return 0;
}

























