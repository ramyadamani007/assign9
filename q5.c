/*****************
	Include
*****************/

#include <stdio.h>
#include <stdlib.h>

/***** Declaration of Structure *****/

struct A
{
	int x;
	char *str;	// (or) char str[20];
};

/**** MAIN ****/

int main()
{
	struct A a1 = { 101, "abc" } , a2;
	a1.x=10;
	a1.str="hello";		//works?:-Here, array uses integer value, so this is not used here.
	scanf("%d%s",&a1.x,a1.str);	//works?:-This does not work because we cannot provide input directly to the pointer.
	a2 = a1;			//shallow copy or deep copy?:- It works in every format.
	return 0;
}
