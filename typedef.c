/*****************
	Include
*****************/

#include<stdio.h>
#include<string.h>

/***** Declaration of Structure *****/

typedef struct student		/**** Use of typedef ****/
{
	int id;
	char name [20];
	float percent;
}status;

/**** MAIN ****/

int main()
{
	status record;
	record.id = 1;
	strcpy(record.name,"Ramya");
	record.percent = 96.5;
	printf("The id of student is %d.\nThe name of the student is  %s.\nThe percentage of the student is %f.\n", record.id, record.name, 		record.percent);
	return 0;
}


