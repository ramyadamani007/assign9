/*****************
	Include
*****************/
#include<stdio.h>
#include<stdlib.h>

/***** Declaration of Structure *****/

struct employee
{
	int empid;
	char name[15];
	int joining_year;
	int salary;
};

void service (int[]);	/**** Function Declaration ****/

/**** MAIN ****/

int main()
{
	int total, max, min;
	int total_year, servi[3];
	float avg;
	struct employee e1, e2,e3;
	
	/**** Data entry from user for employee 1 ****/
	
	printf("Enter the employee id, employee name, joining year and salary.\n");
	scanf ("%d",&e1.empid);
	scanf("%s",&e1.name);
	scanf ("%d",&e1.joining_year);
	scanf ("%d",&e1.salary);
	printf("Employee id is %d.\nEmployee name is %s.\nThe year of joining of employee is %d.\nThe salary of respective employee is %d.\n", e1.empid, e1.name, e1.joining_year, e1.salary);
	total_year = 2021-(e1.joining_year);
	servi[0] = total_year;
	printf("\n\n");
	
	/**** Data entry from user for employee 2 ****/
	
	printf("Enter the employee id, employee name, joining year and salary.\n");
	scanf ("%d",&e2.empid);
	scanf("%s",&e2.name);
	scanf ("%d",&e2.joining_year);
	scanf ("%d",&e2.salary);
	printf("Employee id is %d.\nEmployee name is %s.\nThe year of joining of employee is %d.\nThe salary of respective employee is %d.\n", e2.empid, e2.name, e2.joining_year, e2.salary);
	total_year= 0;
	total_year = 2021-(e2.joining_year);
	servi[1] = total_year;
	printf("\n\n");
	
	/**** Data entry from user for employee 3 ****/
	
	printf("Enter the employee id, employee name, joining year and salary.\n");
	scanf ("%d",&e3.empid);
	scanf("%s",&e3.name);
	scanf ("%d",&e3.joining_year);
	scanf ("%d",&e3.salary);
	printf("Employee id is %d.\nEmployee name is %s.\nThe year of joining of employee is %d.\nThe salary of respective employee is %d.\n", e3.empid, e3.name, e3.joining_year, e3.salary);
	total_year= 0;
	total_year = 2021-(e3.joining_year);
	servi[3] = total_year;
	printf("\n\n");
	
	printf("The total of all employee's salary is: ");
	total = (e1.salary + e2.salary + e3.salary);
	printf("%d\n",total);
	
	printf("The average of all employee's salary is: ");
	avg = (total/3);
	printf("%2f\n\n",avg);
	
	max = e1.salary > e2.salary ? (e1.salary > e3.salary ? e1.salary:e3.salary) : (e2.salary > e3.salary ? e2.salary:e3.salary);
	printf("The maximum salary is %d\n",max);
	
	min = e1.salary < e2.salary ? (e1.salary < e3.salary ? e1.salary:e3.salary) : (e2.salary < e3.salary ? e2.salary:e3.salary);
	printf("The minimum salary is %d\n",min);
	service (servi);
}
	
/**** Function Initialization ****/

void service(int a[])
{
	if(a[0]<a[1])
	{  
		if(a[1]<a[2])
		{
			printf("\n Maximum service is e3 %d year",a[2]);
	           	printf("\n Minimum service is e1 %d year",a[0]);
		}
		else
		{
			printf("\n Maximum service is e2 %d year",a[1]);
		        if(a[0]<a[2])
			{
				printf("\n Minimum service is e1 %d year",a[0]);
         		}
          		else
           			printf("\n Minimum service is e3 %d year",a[2]);
         	}
	}
	else 
	{
    		if(a[0]>a[2])
     		{
       		printf("\n Maximum service is e1 %d year",a[0]);
       		if(a[1]>a[2])
         		{
          			printf("\n Minimum service is e3 %d year",a[2]);
          		}
       		else
           		{
           			printf("\n Mimimum service is e2 %d year",a[1]);
           		}
           	}
	}
}
	
	
	
	
	
	
	
	

	
