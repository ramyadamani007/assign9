/*****************
	Include
*****************/

#include <stdio.h>
#include <stdlib.h>

/***** Declaration of Union *****/

union A
{
	int x;
	float y;
	double z;
	int arr[2];
}a1;

/***** MAIN *****/

int main()
{
	a1.y=6.25f;
	printf("x = %x \n",a1.x);
	a1.z=0.15625;
	printf("arr[0] = %x and arr[1] = %x \n",a1.arr[0],a1.arr[1]);
	return 0;
}
