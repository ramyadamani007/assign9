/*****************
	Include
*****************/

#include <stdio.h>
#include <stdlib.h>

/***** Declaration of Structure *****/

typedef struct R 
{
/**** Variable Declaration ****/
	char ch;
	int a;
	float b;
	double c;
		
};

/**** MAIN ****/

int main()
{
	struct R *ptr=0, *p, a1;
	ptr++;
	printf("structure size is %d\n",ptr);
	p=&a1;
	printf("address of stru p is %d\n",p);
	printf("address of stru ch is %c\n",&a1.ch);
	printf("address of stru a is %d\n",a1.a);
	printf("address of stru b is %f\n",a1.b);
	printf("address of stru c is %f\n",a1.c);
	
	return 0;
}












