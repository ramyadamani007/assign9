/*****************
	Include
*****************/
#include<stdio.h>
#include<stdlib.h>

/***** Declaration of Structure *****/

struct student 
{				
/**** Variable Declaration ****/
	int stuid;		
	char sub[10];
	float marks[5];
	float total;
};

/**** MAIN ****/

int main()
{
	int i;
	struct student s1,s2;
	
	/**** Data entry from user for student 1 ****/
	
	printf("Enter the student id and subject.\n");
	scanf ("%d",&s1.stuid);
	scanf("%s",&s1.sub);
	printf("Enter the marks of all 5 subjects.\n");
	for (i=0;i<=4;i++)
		{
		scanf("%f",&s1.marks[i]);
		}
	printf("The total of the all marks of the student is: ");
	i = 0;
	while (i!=5)
		{
		s1.total = s1.total + s1.marks[i];
		i++;
		}
		printf("%f\n",s1.total);
		printf("\n %d %s %f %f\n", s1.stuid, s1.sub, s1.marks[i], s1.total);
	printf("\n");
	
	/**** Data entry from user for student 2 ****/
	
	printf("Enter the student id and subject.\n");
	scanf ("%d",&s2.stuid);
	scanf("%s",&s2.sub);
	printf("Enter the marks of all 5 subjects.\n");
	for (i=0;i<=4;i++)
		{
		scanf("%f",&s2.marks[i]);
		}
	printf("\n %d %s %f\n", s2.stuid, s2.sub, s2.marks[i]);
	printf("The total of the all marks of the student is: ");
	i = 0;
	while (i!=5)
		{
		s2.total = s2.total + s2.marks[i];
		i++;
		}
	printf("%f\n",s2.total);
	printf("\n %d %s %f %f\n", s2.stuid, s2.sub, s2.marks[i], s2.total);
	printf("\n");
	return 0;
}
	
	
	
	
	
	
	
	
	
	
	
