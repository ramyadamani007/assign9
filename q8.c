/*****************
	Include
*****************/

#include <stdio.h>
#include <stdlib.h>

/***** Declaration of Union*****/
union A
{
	int x;
	int y;
	char ch;
};

/**** MAIN ****/

int main()
{
	union A a1;
	int size = sizeof(a1);
	printf("The size of union is %d.\n",size);
	return 0;
}
