/*****************
	Include
*****************/

#include <stdio.h>
#include <stdlib.h>

/***** Declaration of Union *****/

union B
{
	int x;
	short int y;
	char ch;
	char carr[4];
}b1;

/***** MAIN *****/

int main()
{
	int x=0;
	b1.x=0x41424344;
	printf("b1.y = %hi\n",b1.y);
	printf("b1.ch = %c\n",b1.ch);
	while (x!=4)
	{
		printf("carr[4] = %c\n",b1.carr[x]);
		x++;
	}
	b1.y=10;
	printf("b1.x = %hi\n",b1.x);
	return 0;
}

















